//
// Created by hemil on 07/07/21.
//

#ifndef STFU_TEST_UTIL_H
#define STFU_TEST_UTIL_H

#include <iostream>

/// An object which if you dont call use method, the program terminates.
/// Used for testing. Use can be called any number of times
class EnsureUsed {
    bool used = false;
public:
    void use() {
        used = true;
    }

    ~EnsureUsed() {
        if (!used) {
            /// std::endl used to ensure std::cout is flushed
            std::cerr << "EnsureUsed variable not used" << std::endl;
            std::exit(EXIT_FAILURE);
        }
    }
};

#endif //STFU_TEST_UTIL_H

#define STFU_IMPL

#include <iostream>
#include <cassert>
#include <stfu/stfu.h>
#include <map>

/**
 * Used later for testing stfu_debug_string. Ignore for now
 */
struct foo {};

std::stringstream &operator<<(std::stringstream &ss, const foo &) {
    ss << "foo";
    return ss;
}

// Used later
struct baz {
};

std::string stfu_debug_string(const baz &) {
    return "baz";
}

int main() {
    int parent = 0, child1 = 0, child2 = 0, grandchild1 = 0, grandchild2 = 0, grandchild3 = 0, grandchild4 = 0;
    stfu::test("Parent", [&] {
        parent++;

        stfu::test("Child 1", [&] {
            child1++;

            stfu::test("Grandchild 1", [&] {
                grandchild1++;
            });

            stfu::test("Grandchild 2", [&] {
                grandchild2++;
            });
        });

        stfu::test("Child 2", [&] {
            child2++;

            stfu::test("Grandchild 3", [&] {
                grandchild3++;
            });

            stfu::test("Grandchild 4", [&] {
                grandchild4++;
            });
        });
    });

    /// flush the output for debugging
    std::cout << std::endl;
    assert(grandchild1 == 1);
    assert(grandchild2 == 1);
    assert(grandchild3 == 1);
    assert(grandchild4 == 1);

    assert(child1 == 2);
    assert(child2 == 2);

    assert(parent == 4);

    stfu::test("more tests can be executed", [] {
        stfu::test("1 == 1", [] {
            assert(1 == 1);
        });
    });

    stfu::test("two tests with same name should throw runtime error", [] {
        stfu::test("abc", [] {});
        try {
            stfu::test("abc", [] {});
            expect(false);
        } catch (std::exception &e) {
            std::cerr << e.what() << '\n';
        }
    });

    /// implicitly also tests expect works with booleans
    stfu::test("expect tests", [] {
        stfu::test("check expect failure should throw an assertion exception", [] {
            try {
                expect(false == true);
                assert(false);
            } catch (stfu::impl::assertion_failed &af) {}
        });

        stfu::test("check expect failure does not crash the program", [] {
            try {
                expect(false);
            } catch (std::exception &e) {

            }
        });

        stfu::test("check expect(false) works", [] {
            try {
                expect(false);
                assert(false);
            } catch (stfu::impl::assertion_failed &af) {

            }
        });

        stfu::test("check expect(true) works", [] {
            try {
                expect(true);
            } catch (...) {
                assert(false);
            }
        });

        stfu::test("expect 1 succeeds", [] {
            expect(1);
        });
    });

    stfu::test("expect no exception thrown tests", [] {
        stfu::test("expect_no_exception does nothing when no exception is thrown", [] {
            try {
                expect_no_exception([] {});
                expect(true);
            } catch (...) {
                expect(false);
            }
        });

        stfu::test("expect_no_exception throws assertion_failed when exception is thrown", [] {
            expect_throws(stfu::impl::assertion_failed, [] {
                expect_no_exception([] { throw std::runtime_error(""); });
            });
        });
    });

    stfu::test("expression tests that should not throw", [] {
        stfu::test("expect 10 == 10", [] {
            expect(10 == 10);
        });

        stfu::test("expect 10 != 1", [] {
            expect(10 != 1);
        });

        stfu::test("expect 1 <= 10", [] {
            expect(1 <= 10);
        });

        stfu::test("expect 10 <= 10", [] {
            expect(10 <= 10);
        });

        stfu::test("expect 11 > 10", [] {
            expect(11 > 10);
        });

        stfu::test("expect 10 >= 10", [] {
            expect(10 >= 10);
        });

        stfu::test("expect 11 >= 10", [] {
            expect(11 >= 10);
        });
    });

    stfu::test("expression tests that should throw", [] {
        stfu::test("expect 1 == 10 throws assertion_failed", [] {
            expect_throws(stfu::impl::assertion_failed, [] {
                expect(1 == 10);
            });
        });

        stfu::test("expect 10 != 10 throws assertion_failed", [] {
            expect_throws(stfu::impl::assertion_failed, [] {
                expect(10 != 10);
            });

        });

        stfu::test("expect 10 <= 1 throws assertion_failed", [] {
            expect_throws(stfu::impl::assertion_failed, [] {
                expect(10 <= 1);
            });
        });

        stfu::test("expect 10 > 10 throws assertion_failed", [] {
            expect_throws(stfu::impl::assertion_failed, [] {
                expect(10 > 10);
            });
        });

        stfu::test("expect 1 >= 10", [] {
            expect_throws(stfu::impl::assertion_failed, [] {
                expect(1 >= 10);
            });
        });
    });

    stfu::test("expect_throws tests", [] {
        stfu::test("expect_throws does nothing when exception of the given type is thrown", [] {
            expect_throws(int, [] { throw 0; });
        });

        stfu::test("expect_throws throws AssertionFailure when the callable does not throw the object of the right type",
                   [] {
                       try {
                           expect_throws(std::string, [] { throw 0; });
                           expect(false);
                       } catch (stfu::impl::assertion_failed &e) {
                           expect(e.expected == "std::string");
                           expect(e.actual == "unknown");
                       }
                   });

        stfu::test("expect_throws throws assertion_failed when the callable does not throw", [] {
            try {
                std::vector<int> a;
                expect_throws(std::out_of_range, [&] {});
                expect(false);
            } catch (stfu::impl::assertion_failed &e) {
            } catch (...) {
                expect(false);
            }
        });
    });

    stfu::test("stfu_debug_string test", [] {
        stfu::test("test stfu_debug_string works with integers", [] {
            expect(stfu_debug_string(1) == "1");
            expect(stfu_debug_string(-1) == "-1");
        });

        stfu::test("test stfu_debug_string works with booleans", [] {
            expect(stfu_debug_string(true) == "true");
            expect(stfu_debug_string(false) == "false");
        });

        stfu::test("test stfu_debug_string works with doubles", [] {
            expect(stfu_debug_string(2.1) == "2.1");
            expect(stfu_debug_string(-2.1) == "-2.1");
        });

        stfu::test("test stfu_debug_string works with strings", [] {
            expect(stfu_debug_string("hello world") == "hello world");
        });

        stfu::test("test stfu_debug_string displays ? for types without operator << overload", [] {
            struct bar {
            };
            expect(stfu_debug_string(bar()) == "?");
        });

        stfu::test("test stfu_debug_string operator << for the types that define it", [] {
            expect(stfu_debug_string(foo()) == "foo");
        });

        stfu::test("test stfu_debug_string uses the overload defined for baz", [] {
            expect(stfu_debug_string(baz()) == "baz");
        });

        stfu::test("test stfu_debug_string returns a std::vector enclosed in {} with elements separated by comma", [] {
            std::vector<int> filled_vector = {1, 2, 3, 4};
            expect(stfu_debug_string(filled_vector) == "[1, 2, 3, 4]");

            std::vector<int> empty;
            expect(stfu_debug_string(empty) == "[]");

            std::vector<int> single_element = {1};
            expect(stfu_debug_string(single_element) == "[1]");
        });

        stfu::test("stfu_debug_string calls the overload for pair when the first argument is pair", [] {
            expect(stfu_debug_string(std::pair<int, int>(1, 2)) == "{first: 1, second: 2}");
        });

        stfu::test("test stfu_debug_string works for a map", [] {
            std::map<int, int> filled_map = {
                    {1, 10},
                    {2, 20},
                    {3, 30},
            };

            expect(stfu_debug_string(filled_map) == "[{first: 1, second: 10}, "
                                                            "{first: 2, second: 20}, "
                                                            "{first: 3, second: 30}]");
        });
    });

    expect_no_exception([] {
        stfu::test("test stfu::test catches the exception thrown in the test", [] {
            expect(false);
        }, false);
    });

    parent = 0, child1 = 0, child2 = 0;
    stfu::test("throwing an exception in one test doesnt interfere with other's execution", [&] {
        parent++;

        stfu::test("Child1", [&] {
            child1++;
            expect(false);
        }, false);

        stfu::test("Child2", [&] {
            child2++;
        });
    });

    expect(parent == 2);
    expect(child1 == 1);
    expect(child2 == 1);

    parent = 0;
    stfu::test("Failed expect in parent test should only fire once even if test has mutliple children", [&] {
        parent++;
        stfu::test("Child 1", [] {});
        stfu::test("Child 2", [] {});
        expect(false);
    }, false);

    expect(parent == 1);

    parent = 0, child1 = 0, child2 = 0, grandchild1 = 0, grandchild2 = 0;
    stfu::test("Failed expect in child test should only fire once even if child test has mutliple children", [&] {
        parent++;
        stfu::test("Child 1", [&] {
            child1++;
            stfu::test("Grandchild 1", [&] {
                grandchild1++;
            });
            stfu::test("Grandchild 2", [&] {
                grandchild2++;
            });
            expect(false);
        }, false);

        stfu::test("Child 2", [&] {
            child2++;
        });
    });

    expect(parent == 2);
    expect(child1 == 1);
    expect(child2 == 1);
    expect(grandchild1 == 1);
    expect(grandchild2 == 0);
}

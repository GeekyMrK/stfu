#ifndef STFU_STFU_H
#define STFU_STFU_H

#include <string>
#include <functional>
#include <utility>
#include <vector>
#include <memory>
#include <cassert>
#include <algorithm>
#include <iostream>

#include "expect.h"

namespace stfu {
    /// int is a dummy return value. You are free to ignore this for
    /// single file test cases. But we need to assign some value to
    /// a static variable to be able to call a function in another
    /// translation unit automatically on executing
    int test(const std::string &name, const std::function<void()> &func, bool log_error = true);
}

/// Implementation details. Subject to change
#ifdef STFU_IMPL
/**
 * Contains all the implementation details.
 */
namespace stfu {
    namespace impl {
        /**
         * Represents the state of a test case
         */
        class test_case {
            /**
             * func - The function to execute when running the test case.
             * Corresponds to the lambda that we get in test function
             */
            const std::function<void()> func;


            /**
             * The children of this test case. Stfu sees a n-ary tree
             * of tests. So each node must know its children.
             *
             * Why is this vector of shared_ptr of test case instead of
             * vector of test case? The reporter needs a reference to
             * failed tests so we will need to keep the test case alive
             * until the program ends.
             */
            std::vector<std::shared_ptr<test_case>> children;

            size_t index_of_next_child_to_execute = 0;

            bool first_execution = true;

            /**
             * Will be null if test case is the root test case.
             * Otherwise will point to parent test case
             */
            test_case *parent;

            /**
             * Increments index_of_next_child_to_execute. If this test should no more
             * run and parent is not null, call parent's increment_children_executed
             * to inform it to execute the next child in the next run.
             */
            void increment_children_executed() {
                index_of_next_child_to_execute++;

                /// If the current test should no more run and has a
                /// parent, notify the parent to update its state
                if (!should_run() && parent) {
                    parent->increment_children_executed();
                }
            }

            /**
             * We need to know whether there was an exception during
             * execution because if so, the test should not run again
             * as it has failed.
            */
            bool exception_during_execution = false;

        public:
            /**
              * If true, error is logged otherwise its not logged
              */
            const bool log_error;

            /**
             * Name of the test. Used to uniquely identify a test case
             * among its parents. If a test case with same name is added
             * twice, std::runtime_error will be thrown
             */
            const std::string name;

            test_case(std::string test_name, std::function<void()> test_func, test_case *test_parent,
                      bool log_error) noexcept
                    : func(std::move(test_func)), parent(test_parent), log_error(log_error),
                      name(std::move(test_name)) {}


            /**
             * Adds a child to the children. If a child with same name
             * exists and it is the first execution, throws std::runtime_error
             * If the index of child == index_of_next_child_to_execute, it will run
             * the test case
             */
            void add_child(std::shared_ptr<test_case> child) {
                auto it = std::find_if(
                        children.cbegin(),
                        children.cend(),
                        [&](const std::shared_ptr<test_case> &c) {
                            return c->name == child->name;
                        }
                );

                auto index = size_t(it - children.cbegin());
                bool child_exists = it != children.cend();

                if (!child_exists) {
                    children.push_back(child);
                    index = children.size() - 1;
                }

                /// We assume test cases remain same. That's the contract.
                /// If in the first execution, we find a child exists, its
                /// a human error. Two test cases with same name are present.
                /// Notify the user and bring down the program
                if (child_exists && first_execution) {
                    throw std::runtime_error("Two tests with same name detected");
                }

                if (index == index_of_next_child_to_execute) {
                    if (children[index_of_next_child_to_execute]->should_run()) {
                        children[index_of_next_child_to_execute]->run();
                    }
                }
            }


            /**
             * Specifies whether a test case should run. Test case is only
             * run when should_run returns true
             */
            bool should_run() {
                return (first_execution
                        || index_of_next_child_to_execute < children.size()) && !exception_during_execution;
            }


            /**
             * Runs the test case.
             *
             * This function needs to refer to impl::current_test which
             * is a test_case*. But test_case is not fully defined yet.
             * So we cannot have a pointer to test_case inside test_case
             * unless its fully defined.
             *
             * So I have declared the function over here. It will be defined
             * after impl::current_test
             */
            void run();


            /**
             * Called when one cycle is complete. Notifies children
             * that cycle is complete or notifies parent that they s
             * should execute the next child if any.
             */
            void cycle_complete() {
                first_execution = false;

                if (index_of_next_child_to_execute < children.size()) {
                    children[index_of_next_child_to_execute]->cycle_complete();
                    return;
                }

                if (parent) {
                    parent->increment_children_executed();
                }
            }
        };

        /**
         * Pointer to current test. It is important because we want to add
         * child tests at the correct level of hierarchy.
         */
        test_case *current_test;


        void test_case::run() {
            /// Update impl::current_test because we are running now.
            /// So all nested tests are our children.
            impl::current_test = this;

            try {
                func();
            } catch (std::exception &e) {
                if (log_error) {
                    std::cerr << name << " failed: " << e.what() << '\n';
                }
                exception_during_execution = true;
            } catch (...) {
                if (log_error) {
                    std::cerr << "Unknown exception caught\n";
                }
                exception_during_execution = true;
            }

            /// We have completed running. So all the children left are
            /// our parent's children
            impl::current_test = parent;
            /// we only need to set this on the first iteration of run.
            /// But adding an extra if condition did not make sense as
            /// setting false to false achieves the same effect
            first_execution = false;
        }

        void run_tests(const std::string &name, const std::function<void()> &func, bool log_error) {
//            std::vector<std::shared_ptr<impl::test_case>> failed_tests;

            impl::test_case root(name, func, impl::current_test, log_error);

            /// We might need multiple iterations of root to execute
            /// all test cases as we are only executing 1 leaf at a time
            while (root.should_run()) {
                root.run();
                root.cycle_complete();
            }

            /// After running all the test cases, we are resetting the .
            /// current test. This allows the runner to be called multiple times.
            impl::current_test = nullptr;
        }
    } /// namespace impl

    int test(const std::string &name, const std::function<void()> &func, bool log_error) {
        using namespace stfu::impl;

        if (current_test == nullptr) {
            run_tests(name, func, log_error);
            return 0;
        }

        /// Ensure current test is not null. There is no case in which
        /// it should be null
        assert(current_test != nullptr);

        bool should_log_error = false;
        bool parent_is_logging_error = current_test->log_error;

        //simplified the if block
        if (parent_is_logging_error) {
            should_log_error = log_error;
        }

        current_test->add_child(std::make_shared<test_case>(name, func, current_test, should_log_error));
        return 0;
    }
} /// namespace stfu
#endif /// end if for STFU_IMPL

#endif //STFU_STFU_H

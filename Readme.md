# STFU
STFU is Simple Testing Framework for Unit tests. 

# Installing
Just copy and paste include/stfu folder in your test folder

# Features
- [X] Simple to use and understand
- [X] C++11 compatible
- [X] As few macros and possible. Macros are really hard to debug 
and confuse the tooling. 
- [X] No dependencies
- [X] Header only
- [X] Multi translation unit support

# Sample test case
Let's write a simple unit test for a factorial function.
Factorial of a number is basically that number multiplied
by all numbers that happen before that number until we reach 1.

So -
```
factorial(1) => 1
factorial(2) => 2 * 1 = 2
factorial(3) => 3 * 2 * 1 = 6
factorial(4) => 4 * 3 * 2 * 1 = 24
factorial(5) => 5 * 4 * 3 * 2 * 1 = 120
```


If you look carefully, factorial of 3 is basically 3 * factorial(2)
as factorial(2) => 2 * 1 and so on. So factorial(n) is equal to
n * factorial(n-1). 

This example is usually used to taught recursion to undergrads.
Let's try to implement and test this. The final code can be found in
example/factorial.cpp

```cpp
/// We need this macro to support multiple cpp file linking together.
/// Will get back to this later
#define STFU_IMPL
#include <stfu/stfu.h>

int factorial(int num) {
    /// factorial of negative number doesn't exist
    if (num < 0) {
        throw std::runtime_error("factorial of negative numbers dont exist");
    }
    
    if (num == 1) {
        return 1;
    }
    
    return num * factorial(num - 1);
}

int main() {
    stfu::test("test factorial of 1 is 1", [] {
        expect(factorial(1) == 1);
    });
    
    stfu::test("test factorial of 2 is 1", [] {
        expect(factorial(2) == 2);
    });
    
    stfu::test("test factorial of 5 is 120", [] {
        expect(factorial(5) == 120);
    });
}
```
Running this code produces no output. Which means all the tests passed.
The use case for this library was actually for my side projects. I wanted
something that requires no set up and just runs all the tests and tells me
if any tests fail. If everything is good, do nothing.

That's all good so far. How do we check if the function throws
std::runtime_error when number is negative? Its actually pretty
trivial to implement by hand. Go check out its implementation. 
But its such a common use case that I have included an expectThrows
macro. 

```cpp
stfu::test("test factorial throws std::runtime_error when number is negative", [] {
    expect_throws(std::runtime_error, [] {
        factorial(-1);
    });
});
```

Expect throws is a macro whose first argument is the expected type
of the exception and second is a function that throws the exception.

But there is else something we forgot. What is factorial of 0? 
Math says 1. There are proofs available online. Here is a good video: 
https://www.youtube.com/watch?v=X32dce7_D48

Let's add a test first to check if factorial of 0 is 1 before adding
the implementation. 

```cpp
stfu::test("test factorial of 0 is 1", [] {
    expect(factorial(0) == 1);
});
```

Let's run this. The output is:
> test factorial of 0 is 1 failed: factorial of negative numbers dont exist

The test case failed. The name of the test and the exception message is
displayed. Let's fix the implementation.

```cpp
int factorial(int num) {
    if (num < 0) {
        throw std::runtime_error("factorial of negative numbers dont exist");
    }
    
    if (num <= 1) {
        return 1;
    }
    
    return num * factorial(num - 1);
}
```

Let's run the tests again. No output! All tests passed yay! 


Tests can also be nested inside each other. We can rewrite the tests
to:
```cpp
stfu::test("factorial tests", [] {
    stfu::test("test factorial of 1 is 1", [] {
        expect(factorial(1) == 1);
    });

    stfu::test("test factorial of 2 is 1", [] {
        expect(factorial(2) == 2);
    });

    stfu::test("test factorial of 5 is 120", [] {
        expect(factorial(5) == 120);
    });

    stfu::test("test factorial of 0 is 1", [] {
        expect(factorial(0) == 1);
    });    
});
```

When you do that, the library internally creates a tree of test cases.
What I am doing internally is doing a depth first traversal to each node
of this tree. So, let's say we have this sample test cases:
```cpp
test("Parent",[] {
    std::cout << "Parent\n";
    
    test("Child 1",[] {
        std::cout << "Child 1\n";
    });
    
    test("Child 2",[] {
        std::cout << "Child 2\n";
    });
});
```

This will output -

```
Parent
Child 1
Parent
Child 2
```

Why would I do that? Well simple. It eliminates the need of fixtures.
What we can do is put the set-up in parent test cases and all children
will see the same values of the variables. For example, lets say we have
a bank account class with deposit and withdraw functions -

```cpp
class BankAccount {
    int balance;
public:
    explicit BankAccount(int balance) : balance(balance) {}

    void deposit(int amount) {
        balance += amount;
    }

    void withdraw(int amount) {
        balance -= amount;
    }

    int current_balance() const {
        return balance;
    }
};
```

Let's add a few tests like so:
```cpp
stfu::test("Bank account tests", [] {
     BankAccount bankAccount(100);

     stfu::test("Depositing 100 bucks changes current balance to 200", [&] {
         expect(bankAccount.current_balance() == 100);
         bankAccount.deposit(100);
         expect(bankAccount.current_balance() == 200);
     });

     stfu::test("Withdrawing 100 bucks changes current balance to 0", [&] {
         expect(bankAccount.current_balance() == 100);
         bankAccount.withdraw(100);
         expect(bankAccount.current_balance() == 0);
     });
 });
```

Note:
1. We need to capture bankAccount into the child test (We are 
 capturing by reference in this case).
2. Even though, we captured bank account by reference, both tests got 
 bank account with same balance i.e. 100. That's because, the first
 time parent is run only deposit test case is executed. The parent is
 then executed again but only executing the withdraw test case.
3. Full code can be found at example/bank_acc_test.cpp

This can be a little confusing but if you are familiar with sections
in catch2, this library is exactly a stand-alone implementation of
sections. If not have a look [here](https://catch2.docsforge.com/v2.13.2/tutorial/#test-cases-and-sections): 


# Expect no throws
There is also an expectNoThrows macro which takes in a function
and fails the test if the function throws. For example -


```cpp
stfu::test("expect no throws demo", [] {
    std::vector<int> v = {1, 2, 3, 4};
    
    stfu::test("accessing first element of non empty vector should work", [&] {
        expect_no_exception([&] {
            expect(v.at(1) == 2);
        });
    });
    
    stfu::test("accessing v.at(v.size()) throws std::out_of_range causing expect_no_exception to fail", [&] {
        expect_no_exception([&] {
            int garbage_value = -1;
            expect(v.at(v.size()) == garbage_value);    
        });
    });
});
```

This outputs: 
```
accessing v.at(v.size()) throws std::out_of_range causing expect_no_exception to fail failed: Assertion Failed.
Expected: No exception thrown
Actual: vector::_M_range_check: __n (which is 4) >= this->size() (which is 4)
/home/hemil/c++/stfu/example/expect_no_throws_test.cpp:22
```
Actual is std::exception::what() i.e. the error message. Full code can
be found at example/expect_no_throws_test.cpp


# Note
1. The only contract of the library is that leaf nodes will be executed 
only once per cycle and sibling test cases will get the same environment.
Your only interface with this library is stfu::test function and expect macro.
All other details are inside impl namespace and subject to change.
2. **Do not catch std::exception in tests.** Reason for this is simple.
I raise stfu::impl::assertion_failed in expect, expectThrows and 
expectNoException when they fail. This allows you to these macros
inside a callback/predicate. For example - 
    ```cpp
    std::vector<int> v = {1, 2, 3, 4};
    stfu::test("expect inside predicate", [&] {
        std::for_each(v.begin(), v.end(), [](int v) {
            expect(v <= 3);
        });
    });
    ```
    
    This correctly prints:
    ```
    expect inside predicate failed: Assertion Failed.
    Expected: v <= 3
    Actual: 4 > 3
    /home/hemil/c++/stfu/example/expect.cpp:18
    ```

# Multiple files/translation unit support
It is not practical to put all our tests in one file. So STFU also
supports splitting tests into multiple files. You can declare tests in 
multiple files and link them together. Catch is you need to declare 
STFU_IMPL in one of the files before including stfu.h.

This is why STFU_IMPL needs to be defined in the tests we wrote so far.

Let's have a look at an example -
multiple_files1.cpp
```cpp
#define STFU_IMPL
#include <stfu/stfu.h>

static int _ = stfu::test("file1", [] {
    std::cout << "file 1\n";
});

int main() {}
```
**Note**: 
1. We #define STFU_IMPL **before** the include.
2. Till now, we were writing tests inside main. You can still do that,
 but I wrote the tests outside for consistency with other files.
3. `_` is a static global variable. Static is important. Otherwise, it 
won't compile. static globals are initialized before main is called
4. The order in which the tests are invoked is implementation defined. 
 This is because of what is popularly known as Static initialization order
 fiasco. Which means **you cannot depend on one file running before or 
 after the other**

multiple_files2.cpp
```cpp
#include <stfu/stfu.h>

static int _ = stfu::test("file2", [] {
    std::cout << "file 2\n";
});
```

Output on my machine is - 
```cpp
file 1
file 2
```

It can also be - 
```cpp
file 2
file 1
```
Because of static initialization order fiasco. Full code can be found in
example/multiple_files1.cpp and example/multiple_files2.cpp

# Expect macro
Just some notes about the expect macro. 
- Conditions with && and || are not supported. For example - 
    ```cpp
    expect(1 == 1 && 2 == 2);
    expect((1 == 1) && (2 == 2));
    ```
    These do not work.
    This does work:
    ```cpp
    expect((1 != 1 && 2 == 2));
    ```
    But does not give meaningful output. Here is the output: -
    ```
    unsupported expect uses failed: Assertion Failed.
    Expected: true
    Actual: false
    /home/hemil/c++/stfu/example/expect.cpp:11
    ```
    This code can be found at example/expect.cpp

# Shout-outs
Huge shout-outs to

1. [Alan Mellor](https://www.linkedin.com/in/alan-mellor-15177927/) and 
   [Sean Charles](https://www.linkedin.com/in/sean-charles-a8a98411a/)
   for the code review
2. Phil Nash and all the contributors for creating Catch2.
3. [Ankur Satle](https://www.linkedin.com/in/ankursatle/) for helping
   me find out how to overload a function template for a specific type
   when that function does not take the given type as input.
4. Hipony#4339 from #include C++ discord for teaching me how
   to check if a type is iterable at compile time.
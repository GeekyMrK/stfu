//
// Created by hemil on 11/09/21.
//

#define STFU_IMPL
#include <stfu/stfu.h>

int main() {
    stfu::test("unsupported expect uses", [] {
//        expect(1 == 1 && 2 == 2);
        expect((1 != 1 && 2 == 2));
//        expect(true && false);
    });

    std::vector<int> v = {1, 2, 3, 4};
    stfu::test("expect inside predicate", [&] {
        std::for_each(v.begin(), v.end(), [](int v) {
            expect(v <= 3);
        });
    });
}
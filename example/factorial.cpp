//
// Created by hemil on 11/09/21.
//

#define STFU_IMPL
#include <stfu/stfu.h>

int factorial(int num) {
    if (num < 0) {
        throw std::runtime_error("factorial of negative numbers dont exist");
    }
    if (num <= 1) {
        return 1;
    }
    return num * factorial(num - 1);
}

int main() {
    stfu::test("factorial tests", [] {
        stfu::test("test factorial of 1 is 1", [] {
            expect(factorial(1) == 1);
        });

        stfu::test("test factorial of 2 is 1", [] {
            expect(factorial(2) == 2);
        });

        stfu::test("test factorial of 5 is 120", [] {
            expect(factorial(5) == 120);
        });

        stfu::test("test factorial throws std::runtime_error when number is negative", [] {
            expect_throws(std::runtime_error, [] {
                factorial(-1);
            });
        });

        stfu::test("test factorial of 0 is 1", [] {
            expect(factorial(0) == 1);
        });
    });
}
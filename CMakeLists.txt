cmake_minimum_required(VERSION 3.19)
project(stfu)

set(CMAKE_CXX_STANDARD 11)

include_directories(.)

if (MSVC)
    add_compile_options(/W4 /WX)
else()
    add_compile_options(-Wall -Wextra -pedantic -Werror)
endif()

add_executable(stfu tests/main.cc include/stfu/stfu.h include/stfu/expect.h tests/main2.cpp tests/main3.cpp tests/test_util.h)
target_include_directories(stfu PRIVATE include/)

add_executable(factorial_test example/factorial.cpp)
target_include_directories(factorial_test PRIVATE include/)

add_executable(bank_acc_test example/bank_acc_test.cpp)
target_include_directories(bank_acc_test PRIVATE include/)

add_executable(expect_no_throws example/expect_no_throws.cpp)
target_include_directories(expect_no_throws PRIVATE include/)

add_executable(multiple_files example/multiple_files1.cpp example/multiple_files2.cpp)
target_include_directories(multiple_files PRIVATE include/)

add_executable(expect example/expect.cpp)
target_include_directories(expect PRIVATE include/)